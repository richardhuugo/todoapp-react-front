import React from 'react'
import PageHeader from '../template/pageHeader'
export default props => (
    <div>
        <PageHeader name="About" small="Nós" />
        <h2>Nossa história </h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et maiores iusto cumque consectetur sed doloremque consequuntur! Dicta distinctio ut in explicabo accusamus quis a, labore optio dolores illum vel maiores.</p>
        <h2>Missão e Visão </h2>
         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita earum nisi debitis aliquam aliquid culpa quaerat, facere corrupti laborum unde asperiores facilis qui dicta, dolorem corporis, consequuntur fugit nihil dignissimos.</p>
         <h2>Impresa</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis deserunt ullam totam laboriosam a quo iure! Corrupti esse odit ad aperiam omnis aut cupiditate ab ipsam eum ipsa, quod iure!</p>
    </div>
)