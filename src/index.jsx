import React from 'react'
import ReactDom from 'react-dom'
import { createStore, compose, applyMiddleware,combineReducers} from 'redux'
import {Provider} from 'react-redux'
import ReduxThunk from 'redux-thunk'
import todoReducer from './reducers/todoReducer'
import App from './main/app'


const reducers = combineReducers({
    todoReducer
})

ReactDom.render(
    <Provider store={createStore(reducers,{}, compose(applyMiddleware(ReduxThunk)) )} >
        <App />
    </Provider>
    , document.getElementById('app'))