import React, {Component} from 'react'
import axios from 'axios'
import {connect} from 'react-redux'
import PageHeader from '../template/pageHeader'
import TodoForm from './todoForm'
import TodoList from './todoList'
import {testar} from '../Actions/actionTodo'



const url = "http://localhost:3003/api/todos"

 class Todo extends Component{
    constructor(props){
        super(props)
        this.state = {description:'', lists: []}
        this.handleChange = this.handleChange.bind(this)
        this.handleAdd = this.handleAdd.bind(this)
        this.handleRemove = this.handleRemove.bind(this)
        this.handleMarkAsDone = this.handleMarkAsDone.bind(this)
        this.handleMarkAsPending = this.handleMarkAsPending.bind(this)
        this.handleSearch = this.handleSearch.bind(this)
        this.handleClose = this.handleClose.bind(this)
        this.refresh()
        
       
    }
    refresh(descri = ''){
        
        const search = descri ? `&description__regex=/${descri}/` : ''
        axios.get(`${url}?sort=-createAt${search}`)
        .then(resp =>this.setState({...this.state,  description:descri, lists:resp.data}))
    }

    handleClose(){
        this.refresh('')
    }

    handleChange(e){
        this.setState({...this.state, description: e.target.value})
    }
    handleSearch(){
        this.refresh(this.state.description)
    }

    handleAdd(){
        this.props.testar('ug')
        const description = this.state.description
        axios.post(url, { description })
        .then(resp => this.refresh())
    }
    handleRemove(list){
        axios.delete(`${url}/${list._id}`).then(resp => this.refresh(this.state.description))
    }
    handleMarkAsDone(list){
        axios.put(`${url}/${list._id}`, {...list, done:true})
        .then(resp => this.refresh(this.state.description))
    }
    handleMarkAsPending(list){
        axios.put(`${url}/${list._id}`, {...list, done:false})
        .then(resp => this.refresh(this.state.description))
    }
    render(){
        
        return(
            <div>
                <PageHeader name="Tarefas" small="Cadastro" />
                <h2>{this.props.teste}</h2>
                <TodoForm 
                handleChange={this.handleChange}
                handleSearch={this.handleSearch}
                handleClose={this.handleClose}
                description={this.state.description} handleAdd={this.handleAdd}/>
                
                <TodoList lista={this.state.lists}
                 handleRemove={this.handleRemove}
                handleMarkAsDone={this.handleMarkAsDone}
                handleMarkAsPending={this.handleMarkAsPending}
                />
            </div>
        )
    }
}

const mapStateToProps = state => ({
    teste: state.todoReducer.teste
})

export default connect (mapStateToProps,{testar})(Todo)