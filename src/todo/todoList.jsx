import React from 'react'
import IconButton from '../template/iconButton'


export default props => {
    const renderRow = () =>{
       const lista = props.lista || []
        return lista.map(list => (
            <tr key={list._id}>
                <td  className={list.done ?  'markedAsDone' : ''} >
                    {list.description}
                </td>
                <td>
                    <IconButton style="success" icon="check" hide={list.done}
                    onClick={() => props.handleMarkAsDone(list)} />

                    <IconButton style="warning" icon="undo" hide={!list.done}
                    onClick={() => props.handleMarkAsPending(list)}    />                

                    <IconButton style="danger" icon="trash-o" hide={!list.done}
                    onClick={ () => props.handleRemove(list) }/>

                    
                </td>
            </tr>
        ))
    }
    return(
        
    <table className="table" >
        <thead>
            <tr>
                <th>Descrição</th>
                <th className='tableActions' >Ações</th>
            </tr>
        </thead>
        <tbody>
            {renderRow()}
        </tbody>
  </table>
  
    )
}
  
