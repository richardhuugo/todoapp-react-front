import 'modules/bootstrap/dist/css/bootstrap.min.css'
import 'modules/font-awesome/css/font-awesome.min.css'
import '../template/custom.css'
import React from 'react'
import Routes from './routes'
import Menu from '../template/menu'


export default class App extends React.Component {
    render() {
        return (
            <div className="container">
                <Menu />
                <Routes />
                
            </div>
        )
    }
}